package goodnetz.com.miwokapp;

import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class PhraseActivity extends AppCompatActivity {
    ListView listphrase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phrase);

        ArrayList<Word> arrayWord = new ArrayList<>();
        arrayWord.add(new Word("Where are you going ?","Kemana kamu pergi ?",null, MediaPlayer.create(this, R.raw.phrase_where_are_you_going)));
        arrayWord.add(new Word("What is your name ?","Siapa nama kamu ?",null, MediaPlayer.create(this, R.raw.phrase_what_is_your_name)));
        arrayWord.add(new Word("My name is . .","Namaku adalah",null, MediaPlayer.create(this, R.raw.phrase_my_name_is)));
        arrayWord.add(new Word("How are you feeling ?","Bagaimana Perasaanmu ?",null, MediaPlayer.create(this, R.raw.phrase_how_are_you_feeling)));
        arrayWord.add(new Word("I'm feeling good","Aku merasa baik",null, MediaPlayer.create(this, R.raw.phrase_im_feeling_good)));
        arrayWord.add(new Word("Are you coming ?","Kamu akan datang ?",null, MediaPlayer.create(this, R.raw.phrase_are_you_coming)));
        arrayWord.add(new Word("Yes, I'm coming","Ya, aku akan datang",null, MediaPlayer.create(this, R.raw.phrase_yes_im_coming)));
        arrayWord.add(new Word("I'm Coming","Aku datang",null, MediaPlayer.create(this, R.raw.phrase_im_coming)));
        arrayWord.add(new Word("Let's go","Ayo",null, MediaPlayer.create(this, R.raw.phrase_lets_go)));
        arrayWord.add(new Word("Come here","Datang kemari",null, MediaPlayer.create(this, R.raw.phrase_come_here)));

        listphrase = (ListView) findViewById(R.id.list_phrase);
        WordAdapter wordAdapter = new WordAdapter(this, arrayWord);

        listphrase.setAdapter(wordAdapter);
    }
}
