package goodnetz.com.miwokapp;

import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by 7aufirch on 4/30/2017.
 */

public class Word {
    private String mDefaultName, mTranslation;
    private Drawable mImage;
    private MediaPlayer mVoice;

    public Word(String mDefaultName, String mTranslation, Drawable mImage, MediaPlayer mVoice) {
        this.mDefaultName = mDefaultName;
        this.mTranslation = mTranslation;
        this.mImage = mImage;
        this.mVoice = mVoice;
    }

    public Word(String mDefaultName, String mTranslation, MediaPlayer mVoice) {
        this.mDefaultName = mDefaultName;
        this.mTranslation = mTranslation;
        this.mVoice = mVoice;
    }

    public String getDefaultName() {
        return mDefaultName;
    }

    public void setDefaultName(String mDefaultName) {
        this.mDefaultName = mDefaultName;
    }

    public String getTranslation() {
        return mTranslation;
    }

    public void setTranslation(String mTranslation) {
        this.mTranslation = mTranslation;
    }

    public Drawable getImage() {
        return mImage;
    }

    public void setImage(Drawable mImage) {
        this.mImage = mImage;
    }

    public MediaPlayer getVoice() {
        return mVoice;
    }

    public void setVoice(MediaPlayer mVoice) {
        this.mVoice = mVoice;
    }
}
