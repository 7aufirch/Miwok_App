package goodnetz.com.miwokapp;

import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class NumbersActivity extends AppCompatActivity {
    ListView listword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers);

        ArrayList<Word> arrayWord = new ArrayList<>();
        arrayWord.add(new Word("One","Satu", ContextCompat.getDrawable(this, R.drawable.number_one), MediaPlayer.create(this, R.raw.number_one)));
        arrayWord.add(new Word("Two","Dua", ContextCompat.getDrawable(this, R.drawable.number_two), MediaPlayer.create(this, R.raw.number_two)));
        arrayWord.add(new Word("Three","Tiga",ContextCompat.getDrawable(this, R.drawable.number_three), MediaPlayer.create(this, R.raw.number_three)));
        arrayWord.add(new Word("Four","Empat",ContextCompat.getDrawable(this, R.drawable.number_four), MediaPlayer.create(this, R.raw.number_four)));
        arrayWord.add(new Word("Five","Lima",ContextCompat.getDrawable(this, R.drawable.number_five), MediaPlayer.create(this, R.raw.number_five)));
        arrayWord.add(new Word("Six","Enam",ContextCompat.getDrawable(this, R.drawable.number_six), MediaPlayer.create(this, R.raw.number_six)));
        arrayWord.add(new Word("Seven","Tujuh",ContextCompat.getDrawable(this, R.drawable.number_seven), MediaPlayer.create(this, R.raw.number_seven)));
        arrayWord.add(new Word("eight","Delapan",ContextCompat.getDrawable(this, R.drawable.number_eight), MediaPlayer.create(this, R.raw.number_eight)));
        arrayWord.add(new Word("Nine","Sembilan",ContextCompat.getDrawable(this, R.drawable.number_nine), MediaPlayer.create(this, R.raw.number_nine)));
        arrayWord.add(new Word("Ten","Sepuluh",ContextCompat.getDrawable(this, R.drawable.number_ten), MediaPlayer.create(this, R.raw.number_ten)));



        listword = (ListView) findViewById(R.id.list_word);
        WordAdapter wordAdapter = new WordAdapter(this, arrayWord);
        listword.setAdapter(wordAdapter);
    }
}
