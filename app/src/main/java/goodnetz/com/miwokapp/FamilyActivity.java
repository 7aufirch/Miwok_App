package goodnetz.com.miwokapp;

import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class FamilyActivity extends AppCompatActivity {
    ListView listfamily;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family);

        ArrayList<Word> arrayWord = new ArrayList<>();
        arrayWord.add(new Word("Father","Ayah", ContextCompat.getDrawable(this, R.drawable.family_father), MediaPlayer.create(this, R.raw.family_father)));
        arrayWord.add(new Word("Mother","Ibu", ContextCompat.getDrawable(this, R.drawable.family_mother), MediaPlayer.create(this, R.raw.family_mother)));
        arrayWord.add(new Word("Older Brother","Kakak Laki-laki",ContextCompat.getDrawable(this, R.drawable.family_older_brother), MediaPlayer.create(this, R.raw.family_older_brother)));
        arrayWord.add(new Word("Older Sister","Kakak Perempuan",ContextCompat.getDrawable(this, R.drawable.family_older_sister), MediaPlayer.create(this, R.raw.family_older_sister)));
        arrayWord.add(new Word("Grandfather","Kakek",ContextCompat.getDrawable(this, R.drawable.family_grandfather), MediaPlayer.create(this, R.raw.family_grandfather)));
        arrayWord.add(new Word("Grandmother","Nenek",ContextCompat.getDrawable(this, R.drawable.family_grandmother), MediaPlayer.create(this, R.raw.family_grandmother)));
        arrayWord.add(new Word("Son","Anak Laki-laki",ContextCompat.getDrawable(this, R.drawable.family_son), MediaPlayer.create(this, R.raw.family_son)));
        arrayWord.add(new Word("Doughter","Anak Perempuan",ContextCompat.getDrawable(this, R.drawable.family_daughter), MediaPlayer.create(this, R.raw.family_daughter)));
        arrayWord.add(new Word("Younger Brother","Adik Laki-laki",ContextCompat.getDrawable(this, R.drawable.family_younger_brother), MediaPlayer.create(this, R.raw.family_younger_brother)));
        arrayWord.add(new Word("Younger Sister","Adik Perempuan",ContextCompat.getDrawable(this, R.drawable.family_younger_sister), MediaPlayer.create(this, R.raw.family_younger_sister)));

        listfamily= (ListView) findViewById(R.id.list_family);
        WordAdapter wordAdapter = new WordAdapter(this, arrayWord);
        listfamily.setAdapter(wordAdapter);
    }
}
