package goodnetz.com.miwokapp;

import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class ColorsActivity extends AppCompatActivity {
    ListView listcolor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colors);
            ArrayList<Word> arrayWord = new ArrayList<>();
            arrayWord.add(new Word("Black","Hitam", ContextCompat.getDrawable(this, R.drawable.color_black), MediaPlayer.create(this, R.raw.color_black)));
            arrayWord.add(new Word("Brown","Coklat", ContextCompat.getDrawable(this, R.drawable.color_brown), MediaPlayer.create(this, R.raw.color_brown)));
            arrayWord.add(new Word("Dusty Yellow","Kuning Pasir",ContextCompat.getDrawable(this, R.drawable.color_dusty_yellow), MediaPlayer.create(this, R.raw.color_dusty_yellow)));
            arrayWord.add(new Word("Grey","Abu-abu",ContextCompat.getDrawable(this, R.drawable.color_gray), MediaPlayer.create(this, R.raw.color_gray)));
            arrayWord.add(new Word("Green","Hijau",ContextCompat.getDrawable(this, R.drawable.color_green), MediaPlayer.create(this, R.raw.color_green)));
            arrayWord.add(new Word("Yellow","Kuning",ContextCompat.getDrawable(this, R.drawable.color_mustard_yellow), MediaPlayer.create(this, R.raw.color_mustard_yellow)));
            arrayWord.add(new Word("Red","Merah",ContextCompat.getDrawable(this, R.drawable.color_red), MediaPlayer.create(this, R.raw.color_red)));
            arrayWord.add(new Word("White","Putih",ContextCompat.getDrawable(this, R.drawable.color_white), MediaPlayer.create(this, R.raw.color_white)));

            listcolor= (ListView) findViewById(R.id.list_color);
            WordAdapter wordAdapter = new WordAdapter(this, arrayWord);
            listcolor.setAdapter(wordAdapter);

    }
}
