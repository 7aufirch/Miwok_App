package goodnetz.com.miwokapp;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 7aufirch on 4/30/2017.
 */

public class WordAdapter extends ArrayAdapter<Word> {

    public WordAdapter(Context context, ArrayList<Word> words) {
        super(context, 0, words);
    }
        @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.word_item, parent, false);
        }

        final Word word = getItem(position);
            ImageView image = (ImageView) listItemView.findViewById(R.id.img);
            if (word.getImage()==null){

            }else {

                image.setImageDrawable(word.getImage());
            }
        TextView defaultTextView = (TextView) listItemView.findViewById(R.id.txt_ing);
        defaultTextView.setText(word.getDefaultName());


        TextView translation = (TextView) listItemView.findViewById(R.id.txt_ind);
        translation.setText(word.getTranslation());

            final MediaPlayer mp = word.getVoice();


        listItemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mp.start();
            }
        });

        return listItemView;
    }
}
