package goodnetz.com.miwokapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView numb, fams, color, phrase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //tombol numbers
        numb = (TextView) findViewById(R.id.txt_numbers);
        numb.setOnClickListener(onClickNumbers);

        //tombol family
        fams = (TextView) findViewById(R.id.txt_family);
        fams.setOnClickListener(onClickFamily);

        color = (TextView) findViewById(R.id.txt_colors);
        color.setOnClickListener(onClickColor);

        phrase = (TextView) findViewById(R.id.txt_phrase);
        phrase.setOnClickListener(onClickPhrase);
    }

    private View.OnClickListener onClickNumbers = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent numbersIntent = new Intent(MainActivity.this, NumbersActivity.class);
            startActivity(numbersIntent);
        }
    };

    private View.OnClickListener onClickFamily = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent familyIntent = new Intent(MainActivity.this, FamilyActivity.class);
            startActivity(familyIntent);
//            Intent familyIntent = new Intent(Intent.ACTION_SEND);
//            familyIntent.setType("text/plain");
//            familyIntent.putExtra(Intent.EXTRA_EMAIL, "taufirch@gmail.com");
//            familyIntent.putExtra(Intent.EXTRA_SUBJECT, "Belajar IAK");
//            familyIntent.putExtra(Intent.EXTRA_TEXT, "Guys Belajar Android Yuk");
//            if (familyIntent.resolveActivity(getPackageManager())!=null){
//                startActivity(familyIntent);
//            }else{
//                Toast.makeText(MainActivity.this, "APLIASI TIDAK DITEMUKAN", Toast.LENGTH_SHORT).show();
//            }

        }
    };

    public View.OnClickListener onClickColor = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent colorIntent = new Intent(MainActivity.this, ColorsActivity.class);
            startActivity(colorIntent);
        }
    };

    public View.OnClickListener onClickPhrase= new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent phraseIntent = new Intent(MainActivity.this, PhraseActivity.class);
            startActivity(phraseIntent);
        }
    };

}
